#include "stdafx.h"
#include "MainMenuView.h"
#include "game.h"

MainMenuView::MainMenuView()
: View(Game::WIDTH, Game::HEIGHT)
, imgBackground("Assets/Bitmaps/MainMenuBackground.jpg", 1, 1)
, title("Assets/Fonts/DSDIGI.TTF", 80)
, programmerName("Assets/Fonts/DSDIGI.TTF", 40)
, btnQuit("Assets/Fonts/DSDIGI.TTF", 40, "Assets/Bitmaps/MainMenuQuit.jpg", 300, 75)
, btnConnect("Assets/Fonts/DSDIGI.TTF", 40, "Assets/Bitmaps/MainMenuConnect.jpg", 300, 75)
, btnStartGame("Assets/Fonts/DSDIGI.TTF", 40, "Assets/Bitmaps/MainMenuStartGame.jpg", 300, 75)
, controller(model, *this)
{
  CenterInScreen();
  // Titre
  title.SetPosition(sf::Vector2f(0, 0));  
  title.SetText("A very impressive exercice!");
  title.CenterInScreen(*this);
  ui.AddWidget(title);

  // Programmer name
  programmerName.SetText(model.GetProgrammersName());
  programmerName.SetPosition(sf::Vector2f(0, (float)(GetHeight()-programmerName.GetBounds().height*2.0f)));
  programmerName.CenterInScreen(*this);
  ui.AddWidget(programmerName);

  // BtnConnect
  btnConnect.SetPosition(sf::Vector2f(0, 200));
  btnConnect.CenterInScreen(*this);
  btnConnect.AddListener(std::bind(&MainMenuView::OnBtnConnect, this, std::placeholders::_1), sf::Event::MouseButtonPressed);
  ui.AddWidget(btnConnect);

  // btnQuit
  btnQuit.SetPosition(sf::Vector2f(362, 300));
  btnQuit.CenterInScreen(*this);
  btnQuit.AddListener(std::bind(&MainMenuView::OnBtnQuit, this, std::placeholders::_1), sf::Event::MouseButtonPressed);
  ui.AddWidget(btnQuit);

  // btnStartGame
  btnStartGame.SetPosition(sf::Vector2f(362, 400));
  btnStartGame.CenterInScreen(*this);
  btnStartGame.AddListener(std::bind(&MainMenuView::OnBtnStartGame, this, std::placeholders::_1), sf::Event::MouseButtonPressed);
  ui.AddWidget(btnStartGame);
}


MainMenuView::~MainMenuView()
{
}

void MainMenuView::OnBtnConnect(const Widget& /*w*/)
{
  NotifyObservers(MAIN_MENU_CONNECT);
}

void MainMenuView::OnBtnQuit(const Widget& /*w*/)
{
  NotifyObservers(MAIN_MENU_QUIT);
}

void MainMenuView::OnBtnStartGame(const Widget& /*w*/)
{
  NotifyObservers(MAIN_MENU_START_GAME);
}

void MainMenuView::Update()
{
  programmerName.SetText(model.GetProgrammersName());
}
