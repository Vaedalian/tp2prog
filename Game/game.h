#pragma once
#include "stdafx.h"
#include <string>
#include "LeaderboardView.h"
#include "LeaderboardEntryView.h"
#include "MainMenuView.h"
#include "GameplayView.h"
class Game
{
enum GameState
{
  GS_MAIN_MENU,
  GS_LEADERBOARD
};
public:
  static const unsigned int WIDTH;
  static const unsigned int HEIGHT;
  static const float FRAMERATE;
public:

  Game(std::string windowName,unsigned int width, unsigned int height);
  void Run();

  /// <summary>
  /// Met � jour le jeu
  /// </summary>
  /// <returns>true si le jeu doit se terminer (partie perdue), false sinon</returns>
  bool Update();

private:
  void ShowView(GameState state);
  void ConnectEvents();


  std::map< GameState, View*> views;
  MainMenuView mainMenuView;

	sf::RenderWindow window;


  void OnMainMenuQuit();
  void OnMainMenuStartGame();

	/// <summary>
	/// La largeur du jeu en pixels
	/// </summary>
	int gameWidth;

	/// <summary>
	/// La hauteur du jeu en pixels
	/// </summary>
	int gameHeight;

  std::string gameName;

  GameState currentState;
};

