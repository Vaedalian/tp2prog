#include "stdafx.h"
#include "game.h"
#include "textbox.h"

const unsigned int Game::WIDTH = 1024;
const unsigned int Game::HEIGHT = 768;
const float Game::FRAMERATE = 60.0f;
Game::Game(std::string windowName, unsigned int width, unsigned int height)
  : gameWidth(width)
  , gameHeight(height)
  , gameName(windowName)
  , currentState(GS_LEADERBOARD)
{
}


void Game::Run()
{
  window.create(sf::VideoMode(gameWidth, gameHeight, 32), gameName);
  window.setFramerateLimit(60);
  
  ConnectEvents();
  
  // Initialiser toutes les vues
  views.insert(std::pair(GS_MAIN_MENU, &mainMenuView));

  ShowView(GS_MAIN_MENU);
  while (window.isOpen())
  {
    if (views.find(currentState) != views.end())
      (views.find(currentState)->second)->HandleEvents(window);
    
    // Si fin de partie atteinte,
    if (true == Update())
    {
      // On termine "normalement" l'application
      break;
    }
    else
    {
      window.clear();
      
      if (views.find(currentState) != views.end())
        (views.find(currentState)->second)->Draw(window);

      window.display();
    }

  }
}

bool Game::Update()
{
  static const float DELTA_T = 1.0f / Game::FRAMERATE;
  bool gameMustEnd = false;
  if (nullptr != views[currentState] )
  {    
    views[currentState]->Update(DELTA_T);
  }

  return gameMustEnd;

}

void Game::ShowView(GameState state)
{
  if (nullptr != views[state] && state != currentState)
  {
    if(views[currentState] != nullptr)
      views[currentState]->SetVisible(false);
    currentState= state;
    views[currentState]->Show();
  }
}

void Game::ConnectEvents()
{
  mainMenuView.AddListener(std::bind(&Game::OnMainMenuQuit, this), MAIN_MENU_QUIT);
}

void Game::OnMainMenuStartGame()
{
  //ShowView(GS_PLAYGAME);
}
void Game::OnMainMenuQuit()
{
  window.close(); 
}
