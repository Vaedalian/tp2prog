#pragma once
#include "Widget.h"
class Image : public Widget
{
public:
  Image(const std::string& path, const unsigned int scaleX = 1, const unsigned int scaleY = 1);
  virtual void Draw(sf::RenderWindow& window) const override;
  virtual void SetPosition(const sf::Vector2f& newPos) override;
  virtual sf::Vector2f GetPosition() const override;
private:
  sf::Texture texture;
  sf::Sprite sprite;
};

