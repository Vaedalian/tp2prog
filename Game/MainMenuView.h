#pragma once
#include "View.h"
#include "MainMenuModel.h"
#include "MainMenuController.h"
#include "SlidingButton.h"
#include "Image.h"

#define MAIN_MENU_START_GAME (LAST_VIEW_EVENT+1)
#define MAIN_MENU_QUIT (MAIN_MENU_START_GAME+1)
#define MAIN_MENU_CONNECT (MAIN_MENU_QUIT+1)
class MainMenuView :
  public View
{
public:
  MainMenuView();
  ~MainMenuView();
  virtual void Update()override;
protected:
  // Elements d'interface utilisateur
  Image imgBackground;
  Label title;
  SlidingButton btnConnect;
  SlidingButton btnStartGame;
  SlidingButton btnQuit;
  Label programmerName;
  // Mod�le associ� � la vue
  MainMenuModel model;

  // Contr�leur associ� � la vue
  MainMenuController controller;
private:
  void OnBtnConnect(const Widget& w);
  void OnBtnQuit(const Widget& w);
  void OnBtnStartGame(const Widget& w);
};

