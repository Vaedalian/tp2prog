#pragma once


class MainMenuModel
{
  static const std::string PROGRAMMER_NAME;
public:
  MainMenuModel();
  ~MainMenuModel();
  
  inline const std::string& GetProgrammersName() const{return PROGRAMMER_NAME;}
};

