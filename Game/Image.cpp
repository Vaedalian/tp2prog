#include "stdafx.h"
#include "Image.h"


Image::Image(const std::string& path, const unsigned int scaleX , const unsigned int scaleY)
  : Widget()
{
  texture.loadFromFile( path );
  sprite.setTexture( texture );

  sprite.setScale((float)scaleX, (float)scaleY);
}

void Image::Draw(sf::RenderWindow & window) const
{
  sf::Transform ts;
  if (GetParent() != nullptr)
  {
    ts.translate(GetParent()->GetPosition());
  }
  window.draw( sprite, ts );
}

void Image::SetPosition(const sf::Vector2f & newPos)
{
  sprite.setPosition( newPos );
}

sf::Vector2f Image::GetPosition() const
{
  sf::Vector2f retval = sprite.getPosition();
  if (GetParent() != nullptr)
  {
    retval += GetParent()->GetPosition();
  }
  return retval;

}

